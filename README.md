

```
yarn init
```

##### React
```
yarn add react react-dom react-router-dom -D
```

##### ESLint
```
yarn add eslint eslint-plugin-react -D
```

##### babel
```
yarn add babel-loader @babel/core @babel/preset-env @babel/preset-react @babel/node -D
```

##### babel
```
yarn add webpack webpack-cli webpack-dev-server uglifyjs-webpack-plugin -D
```

##### css
```
yarn add mini-css-extract-plugin node-sass sass-loader style-loader css-loader -D
```

##### dir
```
mkdir public public/js public/css src src/scss src/js
```
