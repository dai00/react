const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

/* Convert import,export Path */
const importJsDirPath    = path.join(__dirname, '/src/js');     // ← 入力フォルダ。watch対象になる。
const importJsFileName   = './index.jsx';                        // ← 入力フォルダ内の、変換で起点になるファイル。
const importScssDirPath  = path.join(__dirname, '/src/scss');   // ← 入力フォルダ。watch対象になる。
const importScssFileName = './index.scss';                      // ← 入力フォルダ内の、変換で起点になるファイル。
const exportJsDirPath    = path.join(__dirname, '/public/js');  // ← 出力フォルダ。
const exportCssDirPath   = path.join(__dirname, '/public/css'); // ← 出力フォルダ。

/* Reverse Proxy */
const proxyIntoHost = 'localhost';
const proxyIntoPort = '3001';

const config = {
  /**
   * JavaScript Convert Config
   * Bundle + Babel + Minify
  */
  JavaScript: {
    context: importJsDirPath,
    entry: {
      bundle: importJsFileName,
    },
    output: {
      path: exportJsDirPath,
      filename: 'bundle.js',      // [name]には、entryのkey(ここでは'bundle')が入る
    },
    optimization: {
      minimizer: [ new UglifyJsPlugin({ uglifyOptions: { compress: { drop_console: true } }, }), ],
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          test: /\.js[x]?$/,
          exclude: /node_modules/,
          options: {
            presets: [
              // プリセットを指定することで、ES2018 を ES5 に変換
              '@babel/preset-env',
              // React の JSX を解釈
              '@babel/react'
            ]
          }
        }
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    /* watch用 dev-server 定義 */
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      host: proxyIntoHost,
      port: proxyIntoPort,
      watchContentBase: true
    },
  },

  /**
   * SCSS Convert Config
   * Bundle + SCSS into CSS + Minify
  */
  Sass: {
    context: importScssDirPath,
    entry: {
      bundle: importScssFileName,
    },
    output: {
      filename: 'bundle.css',
      path: exportCssDirPath,
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader, // javascriptとしてバンドルせず css として出力する
            {
              loader: 'css-loader',
              options: {
                url: false,
                minimize: true, //圧縮
                sourceMap: true //ソースマップツールを有効
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true //ソースマップツールを有効
              }
            }
          ]
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'css/bundle.css'
      })
    ],
    devtool: 'source-map'
  },
};

module.exports = [config.JavaScript, config.Sass];
